// 1. setTimeout() встановлює затримку у виконанні функції, ф-ція виконується один раз, setInterval() встановлює інтервал через який функція буде повторюватись
//2. функція виконається якнайшвидше, але після виконання поточного коду
// 3. тому що функція постійно буде повторюватись


const image = document.getElementsByTagName("img");

let number = 1;

image[0].classList.add("show");

function calc() {

    if (number < 4) {
        number += 1;
    } else {
        number = 1
    }
    console.log(number)
    for (img of image) {
        img.classList.remove("show");
        if (img.dataset.number === String(number)) {
            img.classList.add("show");
        }
    }
}

let interval = setInterval(calc, 3000);
const btnStop = document.createElement("button");
btnStop.textContent = "Припинити";
document.body.append(btnStop);
btnStop.addEventListener("click", stop);

function stop() {
    clearTimeout(interval);
}

const btnStart = document.createElement("button");
btnStart.textContent = "Відновити показ";
document.body.append(btnStart);
btnStart.addEventListener("click", start);

function start() {
    interval = setInterval(calc, 3000);
}